$(document).ready(function () {
    setTimeout(function () {
        $('.loader').hide();
    }, 500);

    var heightMenu = $('#dd-menu').height() - 3;
    $('#play-video-360').click(function () {
        $('#banner-360').toggleClass('show-video')
        $('#dd-menu').addClass('bg-opacity')
    })
    $('.tourist-features').slick({
        slidesToScroll: 3,
        slidesToShow: 3,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        centerPadding: 0,
        centerMode: $(window).width() >= 800,
        touchThreshold: 100,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    centerPadding: 0,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /*** Start -  Full Screen Search */
    var fullScreenSearch = $('.overlay-screen-search'),
        inputShow = $('input.search-input-fake'),
        inputSearch = $('input#search-input'),
        ctrlClose = $('.btn-close-search'),
        isOpen = isAnimating = false,
        toggleSearch = function (evt) {
            if (evt.type.toLowerCase() === 'focus' && isOpen) return false;
            if (isOpen) {
                fullScreenSearch.removeClass('open')
                inputShow.blur();
                $('body').css('overflow', 'inherit')
            } else {
                fullScreenSearch.addClass('open');
                inputSearch.focus()
                $('body').css('overflow', 'hidden')
            }
            isOpen = !isOpen;
        };

    // events
    $(inputShow).focus(toggleSearch);
    $('.icon-search').click(toggleSearch)
    $(ctrlClose).click(toggleSearch);
    // keyboard navigation events
    $(document).keydown(function (ev) {
        var keyCode = ev.keyCode || ev.which;
        if (keyCode === 27 && isOpen) {
            toggleSearch(ev);
        }
    });
    $('.current-lang').click(function (e) {
        e.preventDefault()
        $('.sub-lang').toggle()
    })
    $('.sub-lang a').click(function (e) {
        $('.sub-lang').hide()
        $('.current-lang span').text(e.target.innerText)
    })
    /*** End - Full Screen Search */


    /*** Start - smooth scroll */
    var clickedScroll = false
    $("#dd-menu a.nav-link").click(function (event) {
        event.preventDefault();
        let target = $(this).attr("href");
        $("#dd-menu a.nav-link").removeClass('active')
        $(this).addClass('active')
        try {
            clickedScroll = true
            if ($(window).width() <= 600) {
                $('.navbar-toggle').click();
                $('.navbar-toggler').click();
            }
            $("html,body").stop().animate({
                scrollTop: $(target).offset().top - heightMenu
            }, 1000);
            setTimeout(function () {
                clickedScroll = false
            }, 1000)
        } catch (e) {
            if (target === 'dang-ky.html' || target === 'dang-nhap.html') {
                window.location.replace(target)
            } else {
                window.location.replace('index.html?section=' + target.replace('#', ''))
            }
        }
    });
    /*** End - smooth scroll */

    var stickyNavTop = $('#dd-menu').offset().top;

    var stickyNav = function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > stickyNavTop) {
            $('#dd-menu').addClass('sticky');
        } else {
            $('#dd-menu').removeClass('sticky');
        }
    };
    var scrollTop = $(".scrollTop");
    var displayScrollButton = function () {
        var topPos = $(this).scrollTop();
        // if user scrolls down - show scroll to top button
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");
        } else {
            $(scrollTop).css("opacity", "0");
        }
    }

    stickyNav();
    displayScrollButton();
    // and run it again every time you scroll
    var isScrolledIntoView = function (elem) {
        if (clickedScroll) return false
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();
        if (docViewTop > 4763 && elem === '#map') {
            return true
        }
        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }
    $(window).scroll(function () {
        var menu = ['#about', '#destination', '#events', '#event', '#map']
        menu.forEach(_item => {
            if (!$(_item).length) return
            var isTarget = $('#dd-menu a.nav-link[href="' + _item + '"]')
            if (isScrolledIntoView(_item)) {
                $("#dd-menu a.nav-link").removeClass('active')
                isTarget.addClass('active')
            }
        })
        if ($('#banner-360').length && isScrolledIntoView('#banner-360')) $("#dd-menu a.nav-link").removeClass('active')
        stickyNav();
        displayScrollButton();
    });

    $(scrollTop).click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;

    }); // click() scroll top EMD

    /**
     * Show more about content
     * @param string htmlContent
     */


    $('.icon-scroll-down').click(function () {
        $("html,body").stop().animate({
            scrollTop: $('#about').offset().top - heightMenu
        }, 700);
    })

    /**
     * LOADED PAGE
     * @param string htmlContent
     */
    var GetURLParameter = function (sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
    if (GetURLParameter('section')) {
        setTimeout(function () {
            $("html,body").stop().animate({
                scrollTop: $('#' + GetURLParameter('section')).offset().top - heightMenu
            }, 1000);
        }, GetURLParameter('section') === 'map' ? 1000 : 100)
    }

    /**
     * Set img on view container profile pdf
     * @param string htmlContent
     */

    $('#profile .read-profile').click(function () {
        $('.container-view-pdf').addClass('showed')
    })
    $('#profile .btn-close').click(function () {
        $('.container-view-pdf').removeClass('showed')
    })
    $(window).keyup(function (e) {
        if ($('.container-view-pdf').hasClass('showed')) {
            var keyCode = e.keyCode;
            if (keyCode === 27) $('.container-view-pdf').removeClass('showed')
        }
    });

    /**
     * Phan trang trong muc diem den
     * @param string htmlContent
     */
    var destinationData = [
        {
            img: 'https://hanoimoi.com.vn/Uploads/anhthu/2019/1/30/chu.jpg',
            name: 'Hội chữ Xuân Kỷ Hợi 2019 Văn Miếu',
            page: 1,
            type: 'lehoi'
        },
        {
            img: 'http://www.vietnam-tourism.com//imguploads/tourist/24LHNamdong02JPG.jpg',
            name: 'Lễ hội đình làng Nam Đồng ',
            page: 1,
            type: 'lehoi'
        },
        {
            img: 'http://sovhtt.hanoi.gov.vn/wp-content/uploads/2019/01/ai-7173.jpg',
            name: 'Lễ hội gò Đống Đa ',
            page: 1,
            type: 'lehoi'
        },
        {
            img: 'https://hanoi.gov.vn/image/image_gallery?img_id=2003102798086',
            name: 'Ủy ban nhân dân quận Đống Đa',
            page: 1,
            type: 'coquan'
        },
        {
            img: 'http://cdn.diachiso.vn/images/600x400/2009/4/27/chi-cuc-thue-quan-dong-da.jpg',
            name: 'Chi cục Thuế quận Đống Đa',
            page: 1,
            type: 'coquan'
        },
        {
            img: 'http://img.giaoduc.net.vn/Uploaded/2019/bzivdqmb/2016_07_20/gdvn_anh_tru_so_ca_quan_ddajpg.jpg',
            name: 'Công an quận',
            page: 1,
            type: 'coquan'
        },
        {
            img: 'https://images.foody.vn/res/g11/102376/prof/s576x330/foody-mobile-cv-jpg-255-635524259159481252.jpg',
            name: 'Công Viên Văn Hóa Đống Đa',
            page: 1,
            type: 'giaitri'
        },
        {
            img: 'https://nemtv.vn/wp-content/uploads/2019/01/khu-vui-choi-tiniworlg-nemtv.jpg',
            name: 'Khu vui chơi trẻ em tiNiWorld Nguyễn Chí Thanh',
            page: 1,
            type: 'giaitri'
        },
        {
            img: 'https://chieuphimquocgia.com.vn/Content/Images/uploaded/Gioi%20thieu/16641193303_c1419d4dd3_k.jpg',
            name: 'Rạp chiếu phim quốc gia',
            page: 1,
            type: 'giaitri'
        },
        {
            img: 'https://media.tintucvietnam.vn/uploads/medias/2018/01/24/1024x1024/ngan-hang-doanh-nghiep-bb-baaacYoK7K.JPG?v=1516968361177',
            name: 'Chi nhánh ngân hàng Agribank tại Quận Đống Đa',
            page: 1,
            type: 'dichvu'
        },
        {
            img: 'http://cafefcdn.com/thumb_w/650/2017/httpchannelvcmediavnprupload270201712img201712270915280673-1514344985041.jpg',
            name: 'Chi nhánh ngân hàng Sài Gòn tại Quận Đống Đa',
            page: 1,
            type: 'dichvu'
        },
        {
            img: 'http://i1.topgia.vn/nch/images/2016/10/25/dia-chi-atm-tpbank-dia-diem-dat-cay-atm-ngan-hang-tien-phong-0.jpg',
            name: 'Chi nhánh ngân hàng Tiên Phong bank tại Quận Đống Đa',
            page: 1,
            type: 'dichvu'
        },
        {
            img: 'https://images.foody.vn/res/g7/68777/prof/s640x400/foody-mobile-gril-jpg-908-635990743421962398.jpg',
            name: 'Ngon Restaurant',
            page: 1,
            type: 'nhahang'
        },
        {
            img: 'https://images.foody.vn/res/g12/111511/prof/s640x400/foody-mobile-ga-ran-jpg-177-635984738164899317.jpg',
            name: 'Gà Rán Popeyes',
            page: 1,
            type: 'nhahang'
        },
        {
            img: 'https://images.foody.vn/res/g13/125857/prof/s640x400/foody-mobile-banh-canh-ghe-o-cho--271-635858956469862037.jpg',
            name: 'Bánh Canh Ghẹ 69 - Ô Chợ Dừa',
            page: 1,
            type: 'nhahang'
        },
        {
            img: 'https://images.foody.vn/res/g30/292738/prof/s640x400/foody-mobile-432-jpg-242-636143691704285196.jpg',
            name: 'Vincom Center Phạm Ngọc Thạch',
            page: 1,
            type: 'trungtam'
        },
        {
            img: 'https://images.foody.vn/res/g19/187230/prof/s640x400/foody-mobile-trung_tam_thuong_mai-521-635835251774883946.jpg',
            name: 'Vincom Center Nguyễn Chí Thanh',
            page: 1,
            type: 'trungtam'
        },
        {
            img: 'https://media.laodong.vn/storage/newsportal/2018/10/5/634418/20160531103010-Parks.jpg?w=888&h=592&crop=auto&scale=both',
            name: 'Trung tâm thương mại Parkson',
            page: 1,
            type: 'trungtam'
        },
        {
            img: 'http://baodulich.net.vn/data/data/haubg/2019/kham-pha/1-2019/Anh%201%20-%20Go%20DD.jpg',
            name: 'Gò Đống Đa',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://imagevietnam.vnanet.vn/Upload//2013/12/26/KT9.jpg',
            name: 'Đài tưởng niệm Khâm Thiên',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://mytourcdn.com/upload_images/Image/Location/16_6_2014/Van-mieu-quoc-tu-giam-ha-noi.jpg',
            name: 'Văn Miếu - Quốc Tử Giám',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://mytourcdn.com/upload_images/Image/Ninh/list%205/23%20Ch%C3%B9a%20%C4%90%C3%B4ng%20T%E1%BA%A1/2.jpg',
            name: 'Chùa Đồng Quan',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://1.bp.blogspot.com/-dkt7rcZAT9I/WaYYBgcLS_I/AAAAAAAAABM/yjYdGDnZHOM90MvLH29jBJViKjuuzL90gCLcBGAs/s1600/den-kim-lien-1.jpg',
            name: 'ĐỀN KIM LIÊN',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://huyenbi.net/uploads/chua-phuc-khanh-2016-03-07.jpg',
            name: 'CHÙA PHÚC KHÁNH',
            page: 1,
            type: 'ditich'
        },
        {
            img: 'https://r-ec.bstatic.com/images/hotel/max1024x768/151/151688185.jpg',
            name: 'Pullman Hanoi',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://r-ec.bstatic.com/images/hotel/max1024x768/103/103109503.jpg',
            name: 'Dream Hotel and Apartment',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://r-ec.bstatic.com/images/hotel/max1024x768/277/27753279.jpg',
            name: 'Bao Son International Hotel',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://r-cf.bstatic.com/images/hotel/max1024x768/118/11821617.jpg',
            name: 'Hacinco Hotel',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://q-cf.bstatic.com/images/hotel/max1024x768/676/67667976.jpg',
            name: 'A1 Hill Hanoi Hotel',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://r-cf.bstatic.com/images/hotel/max1024x768/124/124367344.jpg',
            name: 'Hanoi Emotion Hotel',
            page: 1,
            type: 'khachsan'
        },
        {
            img: 'https://r-cf.bstatic.com/images/hotel/max1024x768/103/103269888.jpg',
            name: 'Hanoi La Rosa Hotel',
            page: 2,
            type: 'khachsan'
        },
    ]
    var fetchDestination = function (type, page = 1) {
        var content = ''
        var findData = destinationData.filter(_item => _item.type === type && _item.page === page)
        var getUrl = function (type) {
            if (['lehoi', 'dichvu'].includes(type)) return type + '.html'
            return 'detail.html'
        }
        findData.forEach((_item) => {
            content += `<div class="col-sm-6 col-md-4">
                    <div class="thumb-item" onclick="window.location.href = '${getUrl(_item.type)}'">
                        <img src="${_item.img}" alt="">
                        <div class="content">
                            <div class="name"><a href="${getUrl(_item.type)}">${_item.name}</a></div>
                            <div class="d-flex">
                                <div class="rating">
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <div class="ml-3">
                                    60 <i class="fas fa-eye"></i>
                                </div>
                                <div class="ml-3">
                                    41 <i class="fas fa-heart"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
        })
        if (destinationData.filter(_item => _item.type === type).length > 6) {
            $('#destination .pagination').show()
        } else {
            $('#destination .pagination').hide()
        }
        $('.result-destination').html(content)
        $('#modalDes .modal-body .row').html(content)
    }
    // Show modal des
    $('#destination .des-types .item').click(function () {
        $('#destination .des-types .item').removeClass('active')
        $(this).addClass('active')
        if ($(window).width() < 600) {
            $('#modalDesLabel').html($(this).find('.des-title')[0].innerHTML)
            fetchDestination($(this).attr('data-type'))
            $('#modalDes').modal('show')
        } else {
            fetchDestination($(this).attr('data-type'))
            $('.des-list-data').show()
            $("html,body").stop().animate({
                scrollTop: $('.des-list-data').offset().top - heightMenu
            }, 700);
        }
    })
    $('#destination .page-link').click(function (e) {
        e.preventDefault()
        $('#destination .page-link').removeClass('active')
        var currentPage = parseInt($(this).data('value'))
        if (currentPage === 0) {
            currentPage = 2
            $('#destination .page-link[data-value="2"]').addClass('active')
            $('#destination .page-link[data-value="-1"]').parent().removeClass('disabled')
            $('#destination .page-link[data-value="0"]').parent().addClass('disabled')
        } else if (currentPage === -1) {
            currentPage = 1
            $('#destination .page-link[data-value="1"]').addClass('active')
            $('#destination .page-link[data-value="0"]').parent().removeClass('disabled')
            $('#destination .page-link[data-value="-1"]').parent().addClass('disabled')
        } else {
            $(this).addClass('active')
        }
        fetchDestination('khachsan', currentPage)

    })

    /**
     * SỰ KIỆN
     * @param string htmlContent
     */
    var runSlideEvent = function () {
        $('.slide-event').slick({
            infinite: false,
            slidesToScroll: 3,
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
    var eventsData = [
        {
            img: 'http://kientruc.vn/wp-content/uploads/2015/10/Kientruc.vn-quy-kien-truc-do-thi-01.jpg',
            name: 'Tuần Văn hóa, thể thao quận Đống Đa ',
            month: 1
        },
        {
            img: 'https://hanoimoi.com.vn/Uploads/anhthu/2019/1/30/chu.jpg',
            name: 'Hội chữ Xuân Kỷ Hợi 2019 Văn Miếu',
            month: 2
        },
        {
            img: 'https://2.bp.blogspot.com/-_aMHGErp8hg/XGLnNkmoWhI/AAAAAAAA4uU/aMsCOmZtCM4n7SdKzEVa7Fue-YkAQ8UGwCKgBGAs/s1600/IMG_20190208_155658.jpg',
            name: 'Lễ kỷ niệm 230 năm chiến thắng Ngọc Hồi - Đống Đa',
            month: 2
        },
        {
            img: 'http://vietucnews.net/wp-content/uploads/2018/07/img_8847_resize.jpg',
            name: 'Lễ hội Hoa anh đào Nhật Bản',
            month: 2
        },
        {
            img: 'http://tuyengiao.vn/Uploads/2018/3/29/ttxvn_ngay_tho_1.jpg',
            name: 'Triển Lãm Hệ Thống Văn Miếu Việt Nam Ở Hà Nội',
            month: 2
        },
        {
            img: 'http://media.kinhtedothi.vn/497/2018/3/1/ra-quan.jpg',
            name: 'Quận Đống Đa ra quân huấn luyện dân quân tự vệ',
            month: 3
        },
        {
            img: 'http://cafefcdn.com/thumb_w/650/2015/7tin-52684-1436328343995.jpg',
            name: 'Hà Nội ưu tiên dùng quỹ đất di dời các cơ quan xây khu vui chơi',
            month: 3
        }
    ]

    var bindDataEvent = function (month) {
        var result = '',
            findData = eventsData.filter(_item => _item.month === month)
        if (findData.length) {
            findData.forEach(_item => {
                if (month === _item.month) {
                    result += `<div class="col">
                    <div class="thumb-item" style="background: url(${_item.img}) no-repeat;background-size: cover;    background-position: center;">
                        <div class="content">
                            <div class="name"><a href="sukien.html">${_item.name}</a></div>
                            <div class="mt-2 d-flex"><i class="fas fa-calendar-alt mr-2"></i> 01/03/2019
                            </div>
                            <div class="mt-1 d-flex"><i class="fas fa-map-marker-alt mr-2"></i> Đống Đa, Hà
                                Nội
                            </div>
                        </div>
                    </div>
                </div>`
                }
            })
            $('.slide-event').html(result)
            if (findData.length > 3) {
                if ($('.slide-event').hasClass('slick-initialized')) {
                    $('.slide-event').slick('unslick')
                }
                runSlideEvent();
                $('.slide-event').slick('refresh');
            } else {
                if ($('.slide-event').hasClass('slick-initialized')) {
                    $('.slide-event').slick('unslick')
                }
                $('.slide-event').empty()
                $('.slide-event').html(result)
            }
        } else {
            result = `<div class="text-center w-100"><h4 >Không có sự kiện nào!</h4></div>`
            $('.slide-event').html(result)
        }
    }
    bindDataEvent(2)
    $('.months .item-month').click(function () {
        $('.months .item-month').removeClass('active')
        $(this).addClass('active')
        var month = $(this).data('value')
        bindDataEvent(month)
    })

    /**
     * TOUR NỔI BẬT
     * @param string htmlContent
     */
    var toursData = [
        {
            name: 'Du lịch tâm linh quận Đống Đa',
            img: 'http://media.kinhtedothi.vn/518/2017/2/2/1.jpg',
            page: 1
        },
        {
            name: 'Tour văn miếu quốc tử giám - công viên thủ lệ',
            img: 'https://mytourcdn.com/upload_images/Image/Location/16_6_2014/Van-mieu-quoc-tu-giam-ha-noi.jpg',
            page: 1
        },
        {
            name: 'Tour Khám Phá Ẩm Thực trong ngày',
            img: 'https://www.dulichvietnam.com.vn/khuyen-mai/wp-content/uploads/2018/10/cac-loai-banh-hue.jpg',
            page: 1
        },
        {
            name: 'Tour Khám Phá Hàng Quán trong ngày',
            img: 'https://thietkenoithatidd.com/wp-content/uploads/2017/11/Consulus_QuanAnNgon_THNC.jpg',
            page: 2
        },
        {
            name: 'Du lịch tham quan Hà Nội 1 ngày',
            img: 'https://www.dulichvietnam.com.vn/kinh-nghiem/wp-content/uploads/2017/11/bai-29-cau-long-bien-0.jpg',
            page: 2
        },
        {
            name: 'Tour Hà Nội 1N: Làng gốm Bát Tràng - Chùa Trấn Quốc - Khu di tích Hồ Chủ tịch - Văn Miếu',
            img: 'https://cdn.yeudulich.com/848x480/media/catalog/product/t/-/t-s-ot-han-13.jpg',
            page: 2
        }
    ]
    var fetchDataTour = function (page = 1) {
        var result = ''
        toursData.forEach((_item, index) => {
            result += `<div class="col-md-4">
                <div class="tour-item">
                    <div class="icon-360"></div>
                    <div class="cover">
                        <img src="${_item.img}"
                             alt="">
                    </div>
                    <div class="content">
                        <h2><a href="tour.html">${_item.name}</a></h2>
                        <div class="rating">
                            <i class="fas fa-star active"></i>
                            <i class="fas fa-star active"></i>
                            <i class="fas fa-star active"></i>
                            <i class="fas fa-star active"></i>
                            <i class="far fa-star"></i>
                        </div>
                        <div class="view mt-1">
                            60 <i class="ml-1 fas fa-eye"></i>
                            <span class="ml-3"> 41 <i class="ml-1 fas fa-heart"></i></span>
                        </div>
                    </div>
                </div>
            </div>`
        })
        $('.tour-result').html(result)
        if ($('.tour-result').children().length > 3 || $(window).width() <= 600) {
            $('.tour-result').slick({
                infinite: true,
                autoplay: true,
                slidesToScroll: 3,
                slidesToShow: 3,
                responsive: [
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
    }
    fetchDataTour();

});
var showedFilter = true
var showFilterMap = function (e) {
    showedFilter = !showedFilter
    var content = showedFilter ? '<i class="fas fa-chevron-left"></i>' : '<i class="fas fa-list"></i>'
    $('#map .btn-show').html(content)
    $(".categoy-map").animate({opacity: 'toggle'}, 350);
};
