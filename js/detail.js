$(document).ready(function () {
    var $sticky = $('.post-other'),
        $stickyStopper = $('.dd-post-other').length ? $('.dd-post-other') : $('.weather'),
        contentHeight = $('.post-content').height(),
        sidebarHeight = $('.post-info').height() + $('.post-other').height();
    if (!!$sticky.offset() && contentHeight > sidebarHeight) {
        var stickyTop = $sticky.offset().top;
        $(window).scroll(function () {
            console.log(contentHeight, sidebarHeight)
            var generalSidebarHeight = $sticky.innerHeight();
            var generalSidebarWidth = $sticky.innerWidth();
            var stickOffset = 10;
            var stickyStopperPosition = $stickyStopper.offset().top;
            var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
            var contentTop = $('.wrapper').offset().top;

            var windowTop = $(this).scrollTop();
            if (stopPoint < windowTop) {
                $sticky.css({position: 'absolute', top: 'auto', bottom: '0px'});
            } else if (stickyTop < windowTop + stickOffset) {
                $sticky.css({position: 'fixed', width: generalSidebarWidth, top: stickOffset, bottom: 'auto'});
            } else {
                $sticky.css({position: 'absolute', top: 'initial'});
            }

            if (windowTop > contentTop && windowTop < stickyStopperPosition) {
                $('.float-btn').addClass('fixed')
            } else {
                $('.float-btn').removeClass('fixed')
            }
        });
    }
    $('#page-detail .btn-action').click(function () {
        $('.modal-review').toggle()
    })
    if ($('#page-detail .post-videos .item').length < 3) {
        $('#btn-nav').hide()
        var fist_item = $('.slider-nav .item')[0]
        fist_item = $(fist_item)[0]
        $(fist_item).addClass('active')
        jwplayer('preview-video').setup({
            file: './files/' + $(fist_item).data('url'),
            image: $(fist_item).data('thumb'),
            autostart: false
        });
        $('.slider-nav .item').click(function () {
            var self = $(this)[0]
            $('.slider-nav .item').removeClass('active')
            $(self).addClass('active')
            jwplayer('preview-video').setup({
                file: './files/' + $(self).data("url"),
                image: $(self).data("thumb"),
                autostart: true
            });
        })
    } else {
        $('.slider-nav').slick({
            centerMode: true,
            centerPadding: 0,
            slidesToShow: 3,
            arrows: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                }
            ]
        });
        var showPreview = function () {
            var current = $('.slick-current')[0];
            jwplayer('preview-video').setup({
                file: './files/' + $(current).data("url"),
                image: $(current).data("thumb"),
                autostart: true
            });
        }
        showPreview()
        $('.dd-slick-next').click(function () {
            $('.slider-nav').slick('slickNext');
            showPreview();
        })
        $('.dd-slick-prev').click(function () {
            $('.slider-nav').slick('slickPrev');
            showPreview();
        })
        $('.slider-nav .item').click(function () {
            var self = $(this)[0]
            $('.slider-nav').slick('slickGoTo', $(self).data("slick-index"));
            showPreview();
        })
    }
});
$(document).on("click touchstart", function (e) {
    var container = $(".float-btn");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $('.modal-review').hide();
    }
});
var getdiadiemganday = function () {
    var diadiem = [
            {
                img: 'https://media.tintucvietnam.vn/uploads/medias/2018/01/24/1024x1024/ngan-hang-doanh-nghiep-bb-baaacYoK7K.JPG?v=1516968361177',
                name: 'Chi nhánh ngân hàng Agribank tại Quận Đống Đa',
                page: 1,
                type: 'dichvu'
            },
            {
                img: 'https://images.foody.vn/res/g7/68777/prof/s640x400/foody-mobile-gril-jpg-908-635990743421962398.jpg',
                name: 'Ngon Restaurant',
                page: 1,
                type: 'nhahang'
            },
            {
                img: 'https://images.foody.vn/res/g30/292738/prof/s640x400/foody-mobile-432-jpg-242-636143691704285196.jpg',
                name: 'Vincom Center Phạm Ngọc Thạch',
                page: 1,
                type: 'trungtam'
            },
            {
                img: 'https://mytourcdn.com/upload_images/Image/Ninh/list%205/23%20Ch%C3%B9a%20%C4%90%C3%B4ng%20T%E1%BA%A1/2.jpg',
                name: 'Chùa Đồng Quan',
                page: 2,
                type: 'ditich'
            },
            {
                img: 'https://r-ec.bstatic.com/images/hotel/max1024x768/103/103109503.jpg',
                name: 'Dream Hotel and Apartment',
                page: 2,
                type: 'khachsan'
            },
            {
                img: 'https://r-ec.bstatic.com/images/hotel/max1024x768/277/27753279.jpg',
                name: 'Bao Son International Hotel',
                page: 2,
                type: 'khachsan'
            }
        ],
        result = ''
    diadiem.forEach(_item => {
        result += `<div class="post-item">
                                <div class="item-thumb">
                                    <a href="#"><img
                                            src="${_item.img}"
                                            alt=""></a>
                                </div>
                                <div class="item-content">
                                    <a href="">${_item.name}</a>
                                    <div class="view-heart">
                                        <span class="mr-4"> 50 <i class="fas fa-eye"></i></span>
                                        <span> 20 <i class="fas fa-heart"></i></span>
                                    </div>
                                </div>
                            </div>`
    })
    $('.list-post').html(result)
}
getdiadiemganday()
var fetchData = function (page = 1) {
    var result = ''
    var findData = data.filter(_item => _item.page === page)
    findData.forEach((_item) => {
        result += `<div class="col-sm-6 col-md-4">
                    <div class="thumb-item">
                        <img src="${_item.img}" alt="">
                        <div class="content">
                            <div class="name"><a href="#">${_item.name}</a></div>
                            <div class="d-flex">
                                <div class="rating">
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="fas fa-star active"></i>
                                    <i class="far fa-star"></i>
                                </div>
                                <div class="ml-3">
                                    60 <i class="fas fa-eye"></i>
                                </div>
                                <div class="ml-3">
                                    41 <i class="fas fa-heart"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
    })
    $('.dd-post-other .result').html(result)
}
$('.dd-post-other .page-link').click(function (e) {
    e.preventDefault()
    $('.dd-post-other .page-link').removeClass('active')
    var currentPage = parseInt($(this).data('value'))
    if (currentPage === 0) {
        currentPage = 2
        $('.dd-post-other .page-link[data-value="2"]').addClass('active')
        $('.dd-post-other .page-link[data-value="-1"]').parent().removeClass('disabled')
        $('.dd-post-other .page-link[data-value="0"]').parent().addClass('disabled')
    } else if (currentPage === -1) {
        currentPage = 1
        $('.dd-post-other .page-link[data-value="1"]').addClass('active')
        $('.dd-post-other .page-link[data-value="0"]').parent().removeClass('disabled')
        $('.dd-post-other .page-link[data-value="-1"]').parent().addClass('disabled')
    } else {
        $(this).addClass('active')
    }
    fetchData(currentPage)

})
